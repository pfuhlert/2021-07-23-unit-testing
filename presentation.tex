\documentclass[aspectratio=1610, xcolor={dvipsnames}]{beamer}

\usepackage{amsmath,amsfonts,amsthm,bm}
\usepackage{animate}
\usepackage{booktabs}
\usepackage[english]{babel}
\usepackage{caption}
\usepackage{csquotes}
\usepackage{fontawesome}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{romannum}
\usepackage{siunitx}
\usepackage{tikz}
\usetikzlibrary{arrows.meta, fit, calc, matrix,arrows,positioning, chains, decorations.pathreplacing}

\usepackage{tikzscale}
\usepackage{standalone}

\usepackage{pgfplots}
\usepackage[sfdefault]{Fira Sans}
\usepackage[mathrm=sym]{unicode-math}
% \usefonttheme{professionalfonts}
\setmathfont{Fira Math}

\usepackage{verbatim}

% ### Experimental ###
% \usepackage{subfig}
% \includeonlyframes{current}
% ### End Experimental ###

% ### Theme ###

\usetheme{metropolis}
\metroset{subsectionpage=progressbar, numbering=fraction, titleformat=smallcaps}
\title{Unit Testing}
\date{23.07.2021}
\author{Patrick Fuhlert}
\institute{Institute of Medical Systems Biology\\
% Center for Molecular Neurobiology Hamburg\\
% University Medical Center Hamburg-Eppendorf
}
\makeatletter
\input{metropolis_extensions.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\begin{frame}
  \titlepage
  \tikz [remember picture,overlay]
  \node[anchor=east] at ([xshift=-3cm, yshift=3cm]current page.south east)
  {\includegraphics[width=0.45\textwidth]{img/unittests.png}};
  \footnote{https://martinfowler.com/testing/}
\end{frame}

\begin{frame}{Content}
  \setbeamertemplate{section in toc}[sections numbered]
  \small
  \tableofcontents[hideallsubsections]
\end{frame}

\section{Introduction}

\begin{frame}{Why Testing?
    \footnote{Adapted from "Effective Unit Testing" by Eliotte Rusty Harold}}

  \begin{itemize}
    \item We want our code to work!
    \item<2-> We want our code to \alert{keep working}
    \item<2-> We want to develop as fast as possible with \alert{confidence}
    \item<2-> Because everybody makes mistakes, always, no exceptions
  \end{itemize}

\end{frame}

\begin{frame}{Test Types}
  \begin{minipage}{.6\textwidth}
    \begin{figure}
      \includegraphics[width=\textwidth]{img/testing-triangle.png}
    \end{figure}
  \end{minipage}
  \begin{minipage}{.39\textwidth}
    \begin{itemize}
      \item Unit Tests are the basis and also the most important tests
      \item Number of unit tests in the same order as lines of
            production code
      \item Unit tests should not induce any manual steps
    \end{itemize}
  \end{minipage}

\end{frame}

\section{Unit Tests}

\begin{frame}{What is a Unit Test}
  \begin{figure}
    \includegraphics[height=.8\textheight]{img/unittests.png}
  \end{figure}


\end{frame}

\begin{frame}{How should a unit test look?
    \footnote{Loosely adapted from "The Clean Code Talks -- Unit Testing" by Google TechTalks}}
  \begin{itemize}
    \item Small (fast)
    \item Deterministic
    \item Stateless
    \item Isolate if possible
    \item You can (and should) test for values as well as exceptions
  \end{itemize}
\end{frame}

\begin{frame}{How should a unit test look? --- Example}
  \begin{figure}
    \includegraphics[width=\textwidth]{img/unit-test-samples.png}
  \end{figure}
\end{frame}

\begin{frame}{How should a unit test look? --- Example II}
  \begin{figure}
    \includegraphics[width=\textwidth]{img/unit-test-samples-2.png}
  \end{figure}
\end{frame}


\begin{frame}{Break down complexity}
  \begin{itemize}
    \item If you can split up tests, you usually should do it (Babysteps)
    \item Smaller tests force you to get \alert{explicit}
    \item "Does the car work as expected" can be broken down to
          \begin{itemize}
            \item "Does front left wheel turn right when steering wheel turned to the right?"
            \item "Does front left wheel turn left when steering wheel turned to the left?"
            \item \dots
          \end{itemize}
    \item If one of these tests fail, no debugger is needed to localize the problem
  \end{itemize}
\end{frame}

\subsection{Dependencies}

\begin{frame}{External Dependencies}
  Code modules might have external dependencies like\footnote{More details: \href{https://levelup.gitconnected.com/unit-testing-in-python-mocking-patching-and-dependency-injection-301280db2fed}{levelup.gitconnected.com/unit-testing-in-python-mocking-patching-and-dependency-injection}}
  \begin{itemize}
    \item local time or date
    \item database
    \item internet
    \item file system
    \item randomness
  \end{itemize}

\end{frame}

\begin{frame}{External Dependencies II}
  Common Strategies include
  \begin{itemize}
    \item Mocking
    \item Patching
    \item Dependency Injection
    \item ...
  \end{itemize}
\end{frame}


\begin{frame}{Dependency Injection --- Example}
  \begin{itemize}
    \item[]<+->
    \begin{figure}
      \includegraphics[height=.4\textheight]{tikz/no-dependency-injection.tex}
    \end{figure}
    \item<+-> How to test if my file name really outputs the correct string?\footnote{Loosely adapted from "The Clean Code Talks -- Unit Testing" by Google TechTalks}
          \lstinputlisting[basicstyle=\scriptsize, language=Python, numbers=left,frame=single]{code/filename.py}
  \end{itemize}
\end{frame}

\begin{frame}{Dependency Injection --- Example}
  \begin{itemize}
    \alt<3->{
      \item Add the external dependency explicitly. This way the testing environment can make sure it behaves in the desired way
      \lstinputlisting[basicstyle=\scriptsize, language=Python, frame=single]{code/filename-di.py}
      \item Now testing becomes trivial:
      \lstinputlisting[basicstyle=\scriptsize, language=Python, frame=single]{code/filename-di-test.py}
      }{
      \item<1->[]
      \begin{figure}
        \includegraphics[height=.4\textheight]{tikz/dependency-injection.tex}
      \end{figure}
      \item<2-> Add the external dependency explicitly. This way the testing environment can make sure it behaves in the desired way
      \lstinputlisting[basicstyle=\scriptsize, language=Python, frame=single]{code/filename-di.py}
    }
  \end{itemize}


\end{frame}


\section{Test Driven Development}

\begin{frame}{Test Driven Development}
  \begin{figure}[]
    \centering
    \includegraphics[height=.6\textheight]{img/gopher.png}
  \end{figure}

  \begin{enumerate}
    \item Write \textcolor{red!80!black}{\textbf{Failing Test}} with new functionality
    \item Make test \textcolor{green!80!black}{\textbf{Succeed}} by changing your code
    \item \textcolor{blue!80!black}{\textbf{Clean Up}} your code and keep the tests passing
  \end{enumerate}

\end{frame}

\begin{frame}{Test Driven Development}
  \begin{figure}
    \includegraphics[width=.8\textwidth]{img/red-green-refactor.png}
  \end{figure}
\end{frame}


\begin{frame}{Continuous Integration}
  \begin{figure}
    \includegraphics[width=\textwidth]{img/ci.png}
  \end{figure}
\end{frame}

\begin{frame}{Continuous Integration II}
  \begin{figure}
    \includegraphics[width=\textwidth]{img/gitlab.png}
  \end{figure}

\end{frame}

\section{Conclusion}

\begin{frame}{Takeaways}
  \begin{itemize}
    \item Try focus your tests on very specific functionality (e.g. mock away distractions)
    \item Unit Tests let you make \alert{guarantees} about your code
    \item You can be more \alert{confident} about code capabilities
    \item Try setting up a test environment for you code. If you need help in setting up, please tell me
    \item The best way to learn unit testing is \alert{practice}
  \end{itemize}
\end{frame}

\begin{frame}{Experience with Unit Testing}
  \begin{itemize}
    \item Did you try unit testing already? What are your experiences?
  \end{itemize}
\end{frame}

\section{Code Kata}

\begin{frame}{Code Kata}

  \begin{minipage}{.35\textwidth}
    \begin{figure}
      \includegraphics[width=\textwidth]{img/warrior.png}
    \end{figure}
  \end{minipage}
  \begin{minipage}{.64\textwidth}
    \begin{itemize}
      \item Katas originated from the practice of attack and defence drills by ancient Chinese martial artists. \alert{Repetition} is key on the way from student to grandmaster
      \item The goal is not the end result, but the \alert{stuff you learn along the way}
      \item Usually simple problems that don't have a single solution (30 minutes)
    \end{itemize}
  \end{minipage}
  \footnote{https://kata-log.rocks/ is a great site with problems to start with}
\end{frame}

\begin{frame}{Code Kata --- Example: FizzBuzz}
  \begin{figure}
    \includegraphics[height=.6\textheight]{img/fizzbuzz.png}
  \end{figure}
  Write a program that prints one line for each number from 1 to 100
  \begin{itemize}
    \scriptsize
    \item For multiples of three print Fizz instead of the number
    \item For the multiples of five print Buzz instead of the number
    \item For numbers which are multiples of both three and five print FizzBuzz instead of the number
  \end{itemize}
\end{frame}

\begin{frame}{Code Kata --- Example: Roman Numerals}
  \begin{minipage}{.3\textwidth}
    \begin{figure}
      \includegraphics[height=.9\textheight]{img/roman-numerals.png}
    \end{figure}
  \end{minipage}
  \begin{minipage}{.69\textwidth}
    \begin{table}[]
      \begin{tabular}{ll}
        numeral & literal \\
        1       & I       \\
        2       & II      \\
        3       & III     \\
        4       & IV      \\
        5       & V       \\
        9       & IX      \\
        10      & X       \\
        21      & XXI     \\
        50      & L       \\
        100     & C       \\
        500     & D       \\
        1000    & M
      \end{tabular}
    \end{table}
  \end{minipage}
\end{frame}

\begin{frame}{Suggestion: Pair Programming}
  \begin{minipage}{.5\textwidth}
    \begin{figure}
      \includegraphics[width=\textwidth]{img/pair-programming.png}
    \end{figure}
    \pause
    \begin{figure}
      \includegraphics[width=\textwidth]{img/red-green-refactor.png}
    \end{figure}
  \end{minipage}
  \pause
  \begin{minipage}{.49\textwidth}
  \begin{itemize}
    \item<+-> A writes first \textcolor{red!80!black}{red test}
    \item<+-> B writes code to make \textcolor{green!80!black}{test pass}
    \item<+-> A \textcolor{blue!80!black}{refactors} A's and B's code
    \item<+-> B writes \textcolor{red!80!black}{red test}
    \item<+-> A writes code to make \textcolor{green!80!black}{tests pass}
    \item<+-> B \textcolor{blue!80!black}{refactors} A's and B's code
    \item<+-> \dots
  \end{itemize}
  \end{minipage}
\end{frame}

\begin{frame}{Suggestion II: Visual Studio Code - Live Share}
  \begin{figure}
    \includegraphics[height=.8\textheight]{img/live-share.png}
  \end{figure}
  \href{https://visualstudio.microsoft.com/services/live-share/}{visualstudio.microsoft.com/services/live-share}
\end{frame}


\end{document}
