import bcrypt
from models import db, User
def insert_user_into_db(username, password):
    password_hash = bcrypt.hashpw(
        password.encode('utf-8'),
        bcrypt.gensalt(12))
    user = User(password=password_hash, username=username)
    db.session.add(user)
    db.session.commit()